#!/usr/bin/env bash
# Example: $ update.sh example.com /path/to/key
# Gandi Reference https://doc.livedns.gandi.net/#recordtype

if [ -z "$1" ]
then
	>&2 echo "Need to provide a domain"
	exit 1
fi
if [ -z "$2" ] || ! [ -f "$2" ]
then
	>&2 echo "Need to provide a path to api key"
	exit 1
fi

echo "Starting DNS Sync"

APIKEY=$(cat "$2")
DOMAIN="$1"
IP=""
n=0

while [ $n -le 5 ] && [ -z "$IP" ]
do
	n=$(( n+1 ))
	case $(shuf -i 1-4 -n1) in
		1) 
			echo "[$n/5] Checking OpenDNS for IP via dig"
			IP=$(dig +short myip.opendns.com @resolver1.opendns.com)
		;;
		2)
			echo "[$n/5] Checking Akamai for IP via dig"
			IP=$(dig whoami.akamai.net @ns1-1.akamaitech.net +short)
		;;
		3)
			echo "[$n/5] Checking Google for IP via dig"
			IP=$(dig txt o-o.myaddr.test.l.google.com @ns1.google.com +short | cut -d "\"" -f2)
		;;
		4)
			echo "[$n/5] Checking Cloudflare for IP via dig"
			IP=$(dig @1.1.1.1 ch txt whoami.cloudflare +short | cut -d "\"" -f2)
	esac
	sleep $n
done

if [ -z "$IP" ]
then
	>&2 echo "Failed to find IP"
	exit 1
fi

echo "IP Acquired ($IP) sending to Gandi"

curl \
	--http2 \
	--tlsv1.2 \
	--silent \
	--request PUT \
	--header "Accept: application/json" \
	--header "Content-Type: application/json" \
	--header "Authorization: Apikey $APIKEY" \
	--data '{"rrset_values":["'$IP'"]}' \
	'https://api.gandi.net/v5/livedns/domains/'$DOMAIN'/records/%40/A'

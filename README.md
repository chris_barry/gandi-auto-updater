# Gandi Auto Updater

A utility to update a DNS record using Gandi's API.

Use at your own risk!

## License
[Creative Commons Zero v1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)
